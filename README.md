# de.civiservice.smarty-security-policy-hook

This extension makes changes to the default Smarty Security Policy, as introduced [in CiviCRM 5.74.4](https://civicrm.org/blog/dev-team/civicrm-5744-5696-esr-security-release)
to address a [security issue](https://civicrm.org/blog/dev-team/civicrm-5744-5696-esr-security-release).

As suggested, the change is made by calling the [hook_civicrm_userContentPolicy hook](https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_userContentPolicy/).

Changes are:
- allow crmAPI

This is an [extension for CiviCRM](https://docs.civicrm.org/sysadmin/en/latest/customize/extensions/), licensed under [AGPL-3.0](LICENSE.txt).

## Known Issues

This disables a security fix. It is therefore a quick solution.

For a better solution, turn your {crmAPI} smarty tokens into [Fuzion Tokens](https://civicrm.org/extensions/fuzion-tokens) or similar.
