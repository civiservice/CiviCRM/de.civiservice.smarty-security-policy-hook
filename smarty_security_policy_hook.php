<?php

require_once 'smarty_security_policy_hook.civix.php';

use CRM_SmartySecurityPolicyHook_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function smarty_security_policy_hook_civicrm_config(&$config): void {
  _smarty_security_policy_hook_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function smarty_security_policy_hook_civicrm_install(): void {
  _smarty_security_policy_hook_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function smarty_security_policy_hook_civicrm_enable(): void {
  _smarty_security_policy_hook_civix_civicrm_enable();
}

function smarty_security_policy_hook_civicrm_userContentPolicy(CRM_Core_Smarty_UserContentPolicy $policy) {
  // Enable access to '{crmAPI}'
  $policy->disabled_tags = array_diff($policy->disabled_tags, ['crmAPI']);
}
